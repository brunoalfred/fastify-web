import { UserDto } from "../dto/users.models";
import shortid from "shortid";
import debug from "debug";

const log: debug.IDebugger = debug("app:in-memory-dao");

class UsersDao {
  private static instance: UsersDao;
  users: Array<UserDto> = [];

  constructor() {
    log("Created a new instance of Users Dao");
  }

  static getInstance(): UsersDao {
    if (!UsersDao.instance) {
      UsersDao.instance = new UsersDao();
    }

    return UsersDao.instance;
  }

//   Create User
  async addUser(user: UserDto){
      user.id = shortid.generate();
      this.users.push();
      return user.id;
  }

// Get Users
  async getUsers(){
      return this.users;
  }


}

export default UsersDao.getInstance;
