export interface UserDto{
    id: string,
    name: string,
    password: string,
    firstName?: string,
    lastName?: string,
    permissionLevel?: string
}